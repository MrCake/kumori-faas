#!/bin/bash

kumorictl init
#Change user for your user
kumorictl login djcuagee

#Important to get cue.mod.pkg
kumorictl fetch-dependencies 

#Setup enviroment
kumorictl config --admission admission-ccmaster.vera.kumori.cloud
kumorictl config --user-domain djcuagee.faas


kumorictl register certificate calccert.wildcard \
  --domain *.vera.kumori.cloud \
  --cert-file ./wildcard.vera.kumori.cloud/wildcard.vera.kumori.cloud.crt \
  --key-file ./wildcard.vera.kumori.cloud/wildcard.vera.kumori.cloud.key


kumorictl register http-inbound calcinb \
  --domain faas-djcuagee.vera.kumori.cloud \
  --cert calccert.wildcard \
  --comment "Faas deployment"


  kumorictl register deployment calcdep \
  --deployment ./manifests/deployment \
  --comment "My deploy"


 kumorictl link calcdep:service calcinb


