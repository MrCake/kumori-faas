# FaaS

## Aviso sobre los commits

Guillermo ha tenido problemas para ejecutar el FaaS, por lo que todas las ejecuciones se han hecho sobre el ordenador de Duero, y por comodidad, tras comprobar que el código iba funcionando, era Duero quien hacía los commits, por eso sale siempre él como autor.

## Topología y diseño

Tiene una estructura de 5 capas:
- Frontend: Contenedor que recibe las peticiónes (este está detrás de un balanceador de carga)
- NATS:     Servidor que permite el paso de mensajes y un sistema de colas entre Frontend y Worker
- Worker:   Ejecutor de funciones, conectado con la base de datos (En este también se hacen las funciones de registrar usuario y función)
- Base de Datos: Base de datos escalable, se almacenan los usuarios (y la acumulación de tiempos de ejecución en ms) y funciones (código, nombre, parámetros y usuario asociado)
- Autoscaler: Autoescala el servicio FaaS desplegado. No se conecta con ningun otro componente (gestiona todo en base a la línea de comandos de kumori)


El servicio esta diseñado y programado con Node.js (con las ventajas y desventajas de este). El lenguaje que se admite para la declaración de codigo en las funciones es el mismo (sin opción a descargar modulos que no vengan por defecto, o sean algunos de los instalados para que el servicio sea funcional).

![Topologia del faas](./images/faas.png)

Aunque en la imagen se muestren clústers de NATS y la DB, en el despliegue actual solo se despliega una instancia (aunque se podrían fácilmente desplegar más), ya que difícilmente en pruebas se pueda llegar a atorar la tasa de peticiones/(unidad de tiempo) que permiten dichos servicios.

## Formato de comunicación

La estructura de la API REST toma las siguientes llamadas:

### Registrar usuario

Petición POST a la url https://faas-djcuagee.vera.kumori.cloud/register_user o en su defecto (https://---/register_user) sustituyendo --- por el dns registrado.

Toma un json, con la siguiente estructura:
```
{
    username: "String"
}
```

Un ejemplo con el comando curl de registro de usuario es:

**Ejemplo**
```
curl -X POST -d '{ "username":"duero" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_user
```
**Salida esperada**
```
New user registered
```


### Registro de funcion

Petición POST a https://faas-djcuagee.vera.kumori.cloud/register_function o en su defecto (https://---/register_function)

Toma un json, con la siguiente estructura:
```
{
    username: "String",                                 // Usuerio al que se le registra la función
    name:     "String",                                 // Nombre de la función
    input:    ["String","String","String"],             // Parametros de la función
    code:     "String"                                  // Codigo de la función
}
```
*El campo 'input' declara el nombre de como se declaran las variables pasadas por parametro a la función.


Algunos ejemplos de registro de funciones son:

**Ejemplo**
```
curl -X POST -d '{ "username":"duero","name":"fun1", "input":["one","two"], "code":"console.log(\"a\");" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -X POST -d '{ "username":"duero","name":"fun2", "input":["three"], "code":"console.log(\"a\");\nconsole.log(\"b\");" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -X POST -d '{ "username":"duero","name":"fun4", "input":["a"], "code":"return a;" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -X POST -d '{ "username":"duero","name":"fun5", "input":["a"], "code":"2rou12r0p1rjpjmp" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -X POST -d '{ "username":"duero","name":"fun6", "input":["a"], "code":"for(i=0; i< a; i++); return a;" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
```

**Salida esperada**
```
    New function registered
    New function registered
    New function registered
    New function registered
    New function registered
```

### LLamada a función

Petición POST a https://faas-djcuagee.vera.kumori.cloud/run_function o en su defecto (https://---/run_function)

Toma un json, con la siguiente estructura:
```
{
    username: "String",                                 // Usuerio al que se le ha registrado la función
    name:     "String",                                 // Nombre de la función a llamar
    arguments:    ["String","String","String"],         // Argumentos pasados a la función a la hora de ejecución (debe coincidir con el input definido de la función)
}
```

*El campo 'arguments' tendra el valor(en String -> "0" para un numero) que se asociara a los parametros (en el mismo orden de declaración);

Ejemplo: Si tenemos una lista 'input' (en registro de función) con el siguiente formato
```
    input: ["variableA", "variableB"]
```
y una lista arguments al llamar a la función:
```
    arguments: ["0", "Hola"]
```

Sera igual que en javascript declararlas así (a la hora de pasarlas a la función):

```
    variableA = 0;
    variableB = "Hola";
```


Algunos ejemplos de llamada a función:

**Ejemplo**
```
curl -X POST -d '{ "username":"duero","name":"fun2", "arguments":[1] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function
curl -X POST -d '{ "username":"duero","name":"fun4", "arguments":[1] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function
curl -X POST -d '{ "username":"duero","name":"fun5", "arguments":[1] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function
curl -X POST -d '{ "username":"duero","name":"fun6", "arguments":[900000000] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function --connect-timeout 100000000 --max-time 200000000
```

**Salida esperada**
```
{"return":{}}
{"return":1}
{"err":"SyntaxError: Invalid or unexpected token"}
{"return":900000000}
```

Devuelve la salida que daría una ejecución normal del codigo javascript en una consola, pero devuelto como respuesta de la petición http

## Pegas, posibles mejoras y algunas decisiones

- Se utiliza un sistema publish subscribe para mandar mensajes a las colas, esto significa que NATS manda trabajos a cualquier worker (incluso el que ya esta trabajando mucho), por lo que enviar varias peticiones(de gran cómputo) a la vez puede causar que estas se queden pilladas sin respuesta.


- Se devuelve el resultado de ejecucion en la propia llamada a la API, esto significa que para funciones de tiempo de ejecución largo puede hacer timeout la petición y no recibir el resultado (aunque este sí haya sido calculado).

- Se despliegan varios frontends (y se autoescalan en misma medida que los workers) solo por el hecho de que se puede (aunque gastaria menos recursos no hacerlo).

- Los registros de usuario y funciones se ejecutan como función en un worker. Esto podría hacerse directamente desde el frontend, pero fue una decisión inicial (pensando inicialmentee que el frontend no sería escalable). Hacerlo desde el frontend evitaría comunicación innecesaría.

- El autoescalado tiene reglas simples. Aumenta el numero de workers y frontends en uno, cada vez que el numero de workers activos supera cierto umbral (umbral > x% [dado por kumorictl]) . 

- El tiempo se acumula en ms (redondeado hacia arriba) de ejecución a los usuarios, no hay ninguna petición para obtener dicho tiempo. Por esto la única forma de obtener dicho parámetro es conectarse a la base de datos y mirarlo en la tabla de usuarios:
**Conexión a la base de datos**
```
{
    #local bash
    kumorictl exec -it calcdep db instance-0 db ./cockroach sql --insecure
    #sql terminal
    select * from faas.users;
}
```
**Salida esperada**
```
{
      username | ms_resource
    -----------+--------------
    duero    |         790
    (1 row)

    Time: 2ms total (execution 1ms / network 1ms)

}
```
- **Importante para despliegue**. El autoescalado tiene el usuario y contraseña de Duero de kumori introducido en texto plano en el contenedor, por lo que si se desea que esta funcionalidad esté presente se deberá editar y volver a subir el contenedor correctamente editado (con manifests actualizados en contenedor y localmente), esto es lioso y puede dar problemas, por lo que se recomienda desplegarlo con la cuenta de Duero a ser posible. 

## Ejecución

Actualmente esta en ejecución en "https://faas-djcuagee.vera.kumori.cloud".
Aún así, para probarlo se puede editar en el fichero ./run_kumori.sh la sentencia de kumorictl login "djcuagee" cambiando djcuagee por tu usario. 

Cuando se ejecute el script (si el entorno que se utiliza no tiene nada desplegado con antelación) se desplegará sobre tu cuenta de Kumori. 


## Estructura del proyecto

- /faas: Código de cada componente y todo lo referente al despliegue en contenedores de todos los componentes del sistema FaaS
- /images: Imágenes usadas en este readme
- /manifests: Manifiestos de Kumori para realizar su despliegue
- /wildcard.vera.kumori.cloud: Certificado para Kumori
- /run_kumori.sh:  Ejecutable bash que despliega el sistema FaaS en Kumori mientras se cambie el usuario de login "djcuagee"
- /clean_kumori.sh: Ejecutable bash que elimina todos los componentes desplegados en kumori por '/run_kumori.sh'
