
// Nats requirements
const nats = require('nats')
const argv = require('minimist')(process.argv.slice(2))

const url = argv.s || nats.DEFAULT_URI
const subject = argv._[0]

// Rest api requirements
var express = require('express');
const { allowedNodeEnvironmentFlags } = require('process');
var app = express();
app.use(express.json());

// Gen uuid for jobs
const { v4: uuidv4 } = require('uuid')



if (!subject) {
    console.log('Usage: node-pub  [-s server] [--creds=filepath] <subject> [msg]')
    process.exit()
}

// Connect to NATS server.
const nc = nats.connect({ url: url, json: true })


// Server API

// REGISTER USER
app.post('/register_user', async (req, res) => {
    console.log("Register user")
    var username;
    if (req.body["username"]) {
        username = req.body.username;
    } else {
        return res.send("ERROR - Request should be like : \n{\n\tusername:String\n}\n")
    }

    var job_id = uuidv4();

    var request = {
        type: "register_user",
        username: username,      // Parameter
        job_id: job_id
    }

    nc.publish(subject, request, () => {
        console.log('Published [' + subject + '] : "' + request + '"')
    })

    const opts = { max: 1 }
    // Job response
    nc.subscribe(job_id, opts, (msg) => {
        res.send(msg.return)
        return
    })

});

// REGISTER FUNCTION
app.post('/register_function', async (req, res) => {
    var name, input, code, username;

    if (req.body["name"] && req.body["input"] && req.body["code"] && req.body["username"]) {
        name = req.body.name;
        code = req.body.code;
        username = req.body.username;

        if (Array.isArray(req.body.input)) {
            input = req.body.input.toString();
        } else {
            return res.send("ERROR - 'input' is an array\n");
        }
    } else {
        return res.send("ERROR - Request should be like : \n{\n\tname : String,\n\tinput:[parameter1,parameter2],\n\tcode:String\n\tusername:String\n}\n")
    }

    var job_id = uuidv4();

    var request = {
        type: "register_function",
        function: req.body,
        job_id: job_id
    }

    nc.publish(subject, request, () => {
   //     console.log('Published [' + subject + '] : "' + request + '"')
    })

    const opts = { max: 1 }
    // Job response
    nc.subscribe(job_id, opts, (msg) => {
        res.send(msg.return)
        return
    })

});

// RUN FUNCTION
app.post('/run_function', async (req, res) => {
    var username, name, arguments;

    if (req.body["username"] && req.body["name"] && req.body["arguments"]) {

        if (Array.isArray(req.body.arguments)) {
        } else {
            return res.send("ERROR - 'arguments' is an array\n");
        }

    } else {
        return res.send("ERROR - Request should be like : \n{\n\tusername:String\n\tname:String\n\targuments:[parameter1,parameter2]\n}\n");
    }

    var job_id = uuidv4();

    var request = {
        type: "run_function",
        function: req.body,
        job_id: job_id
    }

    nc.publish(subject, request, () => {
        //console.log('Published [' + subject + '] : "' + request + '"')
    })

    const opts = { max: 1 }
    // Job response
    nc.subscribe(job_id, opts, (msg) => {
        //console.log(msg)
        res.send(msg.return)
        return
    })

});


app.get('/', (req, res) => {
    return res.send('Faas service is working...');
});

app.listen(80, () =>
    console.log(`Example app listening on port 80`),
);


nc.on('error', (e) => {
    console.log('Error [' + nc.currentServer + ']: ' + e)
    process.exit()
})
