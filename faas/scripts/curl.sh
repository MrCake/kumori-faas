
#Specify input array as string maybe ?
#register user
curl -X POST -d '{ "username":"duero" }' -H "Content-Type: application/json" localhost/register_user

#register function
curl -X POST -d '{ "username":"duero","name":"fun1", "input":["one","two"], "code":"console.log(\"a\");" }' -H "Content-Type: application/json" localhost/register_function
curl -X POST -d '{ "username":"duero","name":"fun2", "input":["three"], "code":"console.log(\"a\");\nconsole.log(\"b\");" }' -H "Content-Type: application/json" localhost/register_function
curl -X POST -d '{ "username":"duero","name":"fun4", "input":["a"], "code":"return a;" }' -H "Content-Type: application/json" localhost/register_function


#call function
curl -X POST -d '{ "username":"duero","name":"fun2", "arguments":[1] }' -H "Content-Type: application/json" localhost/run_function
curl -X POST -d '{ "username":"duero","name":"fun4", "arguments":[1] }' -H "Content-Type: application/json" localhost/run_function


## KUMORI
curl -k -X POST -d '{ "username":"duero" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_user

curl -k -X POST -d '{ "username":"duero","name":"fun1", "input":["one","two"], "code":"console.log(\"a\");" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -k -X POST -d '{ "username":"duero","name":"fun2", "input":["three"], "code":"console.log(\"a\");\nconsole.log(\"b\");" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -k -X POST -d '{ "username":"duero","name":"fun4", "input":["a"], "code":"return a;" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -k -X POST -d '{ "username":"duero","name":"fun5", "input":["a"], "code":"2rou12r0p1rjpjmp" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function
curl -k -X POST -d '{ "username":"duero","name":"fun6", "input":["a"], "code":"for(i=0; i<a; i++); return a;" }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/register_function




curl -k -X POST -d '{ "username":"duero","name":"fun2", "arguments":[1] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function
curl -k -X POST -d '{ "username":"duero","name":"fun4", "arguments":[1] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function
curl -k -X POST -d '{ "username":"duero","name":"fun5", "arguments":[1] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function
curl -k -X POST -d '{ "username":"duero","name":"fun6", "arguments":[900000000] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function --connect-timeout 100000000 --max-time 200000000

while true; do curl -k -X POST -d '{ "username":"duero","name":"fun6", "arguments":[900000000] }' -H "Content-Type: application/json" https://faas-djcuagee.vera.kumori.cloud/run_function --connect-timeout 100000000 --max-time 200000000 2> log & sleep 1; done
