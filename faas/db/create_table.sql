CREATE USER IF NOT EXISTS admin;
CREATE DATABASE faas;
GRANT ALL ON DATABASE faas TO admin;

CREATE TABLE faas.users (
    username STRING PRIMARY KEY,
    ms_resource INT DEFAULT 0
);

CREATE TABLE faas.functions (
    --id SERIAL PRIMARY KEY,
    name STRING,
    input STRING,
    code STRING,

    username STRING REFERENCES faas.users,
    
    CONSTRAINT id_function  PRIMARY KEY (name, username)
    
);


-- Insert into database default functions 
-- Create admin role



