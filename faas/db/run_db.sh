#docker network create -d bridge roachnet

# Links to host db files 
#docker run -d --rm --name=roach1 --hostname=roach1 --net=roachnet -p 26257:26257 -p 8080:8080  -v "${PWD}/cockroach-data/roach1:/cockroach/cockroach-data"  cockroachdb/cockroach:v20.2.0 start --insecure --join=roach1,roach2,roach3
docker run -d --rm --name=roach1 --hostname=roach1 --net=faas -p 26257:26257 -p 8080:8080  cockroachdb/cockroach:v20.2.0 start --insecure --join=roach1,roach2,roach3


docker run -d --rm --name=roach2 --hostname=roach2 --net=faas -v "${PWD}/cockroach-data/roach2:/cockroach/cockroach-data" cockroachdb/cockroach:v20.2.0 start --insecure --join=roach1,roach2,roach3

docker run -d --rm --name=roach3 --hostname=roach3 --net=faas -v "${PWD}/cockroach-data/roach3:/cockroach/cockroach-data" cockroachdb/cockroach:v20.2.0 start --insecure --join=roach1,roach2,roach3


docker exec -it roach1 ./cockroach init --insecure

#docker cp $(pwd)/create_table.sql roach1:/cockroach/create_table.sql

docker exec -i roach1 ./cockroach sql --insecure < /home/mrcake/Cake/masterCC/Tareas/kumori/kumori-faas/faas/db/create_table.sql