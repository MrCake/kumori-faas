// Nats 
const nats = require('nats')
const { callbackify } = require('util')
const argv = require('minimist')(process.argv.slice(2))

// Dispatcher
var run_function = require('./run_function').run_function
var register_user = require('./register_user').register_user
var register_function = require('./register_function').register_function

const { type } = require('os')


// Usage 
// node worker.js subject --queue=myqueue


const url = argv.s || nats.DEFAULT_URI
const creds = argv.creds //Empty
const queue = argv.queue
const subject = argv._[0]


if (!subject || !queue) {
    console.log('Usage: node-sub [--queue=qname]')
    process.exit()
}


// Main

const nc = nats.connect({url:url, json: true})


nc.on('connect', () => {
    const opts = {}
    if (queue) {
        opts.queue = queue
    }
    // Function call return handler to frontend
    function resol(handl,job_id) {
        var request = {return: handl}
        console.log(job_id)
        nc.publish(job_id, request, () => {
            console.log('Job Finished')
        })
    }

    // Recieve frontend petition
    nc.subscribe(subject, opts, (msg) => {
        console.log('Received "' + msg + '"')


        if (msg.type == "register_user") {
            register_user(msg.username, resol,msg["job_id"])
        }else if(msg.type == "register_function"){
            register_function(msg.function,resol, msg["job_id"])
        }else if(msg.type == "run_function"){
            run_function(msg.function,resol,msg["job_id"])
        }

    })

    if (queue) {
        console.log('Queue [' + queue + '] listening on [' + subject + ']')
    }
})


nc.on('error', (e) => {
    console.log('Error [' + nc.currentServer + ']: ' + e)
    process.exit()
})

nc.on('close', () => {
    console.log('CLOSED')
    process.exit()
})