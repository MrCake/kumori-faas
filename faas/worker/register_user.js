module.exports.register_user = function (username,res,job_id) {
    var pg = require('pg');
    var config = require('./db_config').config 
    var pool = new pg.Pool(config);
    
    function callback(err, fac) {
        if (err) {
            if (err.code == 23505) {
                console.log("User '"+ username +"' has already been registered\n");
                return res("User has already been registered\n",job_id);
           }else{
                console.log("Unexpected error (Maybe database is down?)\n");
                return res("Unexpected error (Maybe database is down?)\n",job_id);    
        }
        } else {
            console.log("New user registered -> '"+ username +"'\n");
            return res("New user registered\n",job_id);
        }
    }

    pool.query('INSERT INTO faas.users (username) VALUES ($1);', [username], callback);
}

