module.exports.log_time = function (ms_start, username){
    ms_end = process.hrtime(ms_start);

    // DB UPDATE
    var pg = require('pg');
    var config = require('./db_config').config 
    var pool = new pg.Pool(config);


    function callback_insert(err,fac){

        if (err) {
            console.log(err);
            return     
        
        } else {
            console.log("Resource update for user  -> '"+ username +"' --- "+Math.ceil(ms_end[1] / 1000000)+"ms\n");
            return
        }

    }

    function callback(err,fac){
        if (err) {
            console.log("Error retrieving time"+ username +" "+ ms_end[1] + "\n");
            return
        } else {
            if (fac.rowCount != 1) {
                console.log("Time couldnt be logged, user wasnt found\n");
                return
            } else {
                
                var current_ms = parseInt(fac.rows[0].ms_resource);
                var updated = current_ms + Math.ceil(ms_end[1] / 1000000);

                pool.query('UPDATE faas.users SET ms_resource = $1 WHERE username = $2;', [updated,username], callback_insert);
                
                
		        return            
            }

        }
    }

    pool.query("select ms_resource from faas.users where username = $1;", [username], callback);
    return 
}