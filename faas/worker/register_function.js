module.exports.register_function = function (fun, res,job_id) {
    var pg = require('pg');
    var config = require('./db_config').config 
    var pool = new pg.Pool(config);
    
    var name, input, code, username;

    name = fun.name;
    code = fun.code;
    username = fun.username;

    input = fun.input.toString();

    function callback(err, fac) {
        if (err) {
            if (err.code == 23505) {
                console.log(username + " tried to register already existing function");
                return res("Function under this username has already been registered\n",job_id);
            } else if (err.code == 23503) {
                console.log("User who tried to register function dosent exist");
                return res("User does not exist\n",job_id);
            }else{
                console.log("Unexpected error (Maybe database is down?)\n");
                return res("Unexpected error (Maybe database is down?)\n",job_id);    
            }
        } else {
            console.log(username + " registered a new function\n");
            return res('New function registered\n',job_id);
        }
    }

    pool.query('INSERT INTO faas.functions (name, input, code, username) VALUES ($1, $2, $3, $4);', [name, input, code, username], callback);

}