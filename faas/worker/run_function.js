module.exports.run_function = function (fun, res, job_id) {
    // DB Conexion

    var pg = require('pg');
    var config = require('./db_config').config
    var log_time = require('./resource_timer').log_time
    var pool = new pg.Pool(config);

    // Read dispatch parametrs from json 
    var username, name, input;

    username = fun.username;
    name = fun.name;
    input = fun.arguments

    // Run a function retrieved from DB
    function run(input_names, input, code) {
        // set definition for what variables cant be used  // cant use try or explicit words
        try {
            let body = "function(" + input_names.toString() + "){" + code + "}";
            let wrap = s => "{ return " + body + " };"
            var func = new Function(wrap(body));

            var ms_start = process.hrtime();

            var return_value = func.call(null).call(null, ...input);
            log_time(ms_start, username);
            console.log("Function execution success: { return:" + return_value + " }\n");
            if (typeof return_value === 'undefined') {
                res({ return: {} },job_id);
            } else {
                res({ return: return_value }, job_id);
            }
        } catch (err) {
            log_time(ms_start, username);
            console.log("Function execution exception : --> " + err + " <-- }\n");
            res({ err: err.toString() }, job_id);
        }
    }

    // Retrieve parameters from db
    function callback(err, fac) {
        if (err) {
            console.log("Unexpected error, please contact with an admin\n");
            return res("Error\n", job_id);
        } else {
            if (fac.rowCount != 1) {
                console.log("User or function does not exist\n");
                return res("User or function does not exist\n", job_id);
            } else {
                input_names = fac.rows[0].input.split(",")
                code = fac.rows[0].code

                if (input.length != input_names.length) {
                    console.log("Unsuficient input parameteres\n");
                    res("Wrong number of arguments, function has parameters(" + input_names.toString() + ")\n", job_id);
                }
                run(input_names, input, code);
            }

        }
    }

    pool.query("select input,code from faas.functions where name = $1 and username= $2;", [name, username], callback);

}