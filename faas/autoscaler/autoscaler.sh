#!/bin/bash

kumorictl init
kumorictl config --admission admission-ccmaster.vera.kumori.cloud
kumorictl config --user-domain djcuagee.faas

instance=3      #Number of initial instances
time_down=$SECONDS


while true; 
do 
    #Install expect and kumori
    
    #Refresh login
    ./login.sh > /dev/null
    res=$(kumorictl describe deployment calcdep | grep worker | tail -n +2 | awk -F "|" ' {print $6}' | grep -o  [0-9]*)
    arr=($res)
    
    n_zeros=0           # Counts number of workers with no process working

    for i in "${arr[@]}"; do
        (( i < 1 )) && n_zeros=$(($n_zeros+1))
    done

    echo Empty processes $n_zeros
    if [ "$n_zeros" -lt 2 ] && [ "$instance" -lt 10 ]; then                # If all are ocupied - 1 -> increase
        echo "Scale UP --> Instances: "$instance
        sed -i 's/frontend: *rsize: *\$_instances: *[0-9]*/frontend: rsize: \$_instances: '$(($instance+1))'/g' ./manifests/deployment/manifest.cue
        sed -i 's/worker: *rsize: *\$_instances: *[0-9]*/worker: rsize: \$_instances: '$(($instance+1))'/g' ./manifests/deployment/manifest.cue
        instance=$(($instance+1))

       kumorictl update deployment calcdep \
           --deployment ./manifests/deployment \
           --comment "Scale up"
    fi
    
    if [ "$n_zeros" -gt 3 ] && [ $(($SECONDS - time_down)) -gt 100 ]; then        #If 4 or more inactive -> decrease   (100s has to pass since last decrease so it updates properly)
        time_down=$SECONDS
        echo "Scale DOWN --> Instances: "$instance
        sed -i 's/frontend: *rsize: *\$_instances: *[0-9]*/frontend: rsize: \$_instances: '$(($instance-1))'/g' ./manifests/deployment/manifest.cue
        sed -i 's/worker: *rsize: *\$_instances: *[0-9]*/worker: rsize: \$_instances: '$(($instance-1))'/g' ./manifests/deployment/manifest.cue
        instance=$(($instance-1))
 
         kumorictl update deployment calcdep \
           --deployment ./manifests/deployment \
           --comment "Scale down"
    fi
    

    sleep 1; 
done