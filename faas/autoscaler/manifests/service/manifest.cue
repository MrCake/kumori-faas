package calculator

import (
  k        "kumori.systems/kumori/kmv"
  frontend "kumori.systems/examples/calculator/components/frontend"
  nats    "kumori.systems/examples/calculator/components/nats"
  worker  "kumori.systems/examples/calculator/components/worker"
  db       "kumori.systems/examples/calculator/components/db"
  slavedb  "kumori.systems/examples/calculator/components/slavedb"
  autoscaler "kumori.systems/examples/calculator/components/autoscaler"
)

let mefrontend = frontend.#Manifest
let menats = nats.#Manifest
let meworker = worker.#Manifest
let medb = db.#Manifest
let meslavedb = slavedb.#Manifest
let meautoscaler = autoscaler.#Manifest

#Manifest: k.#ServiceManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "calccache"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: {
        service: {
          protocol: "http"
          port: 80
        }
      }
    }

    config: {
      parameter: {
        frontend  : mefrontend.description.config.parameter
        nats    : menats.description.config.parameter
        worker  : meworker.description.config.parameter
        db      : medb.description.config.parameter
        slavedb : meslavedb.description.config.parameter
        autoscaler: meautoscaler.description.config.parameter
      }
      resource: mefrontend.description.config.resource
    }

    // Config spread
    role: {
      frontend: k.#Role
      frontend: artifact: mefrontend
      frontend: cfg: parameter: config.parameter.frontend

      nats: k.#Role
      nats: artifact: menats
      nats: cfg: parameter: config.parameter.nats

      worker: k.#Role
      worker: artifact: meworker
      worker: cfg: parameter: config.parameter.worker

      db : k.#Role
      db : artifact: medb
      db : cfg: parameter: config.parameter.worker

      slavedb : k.#Role
      slavedb : artifact: meslavedb
      slavedb : cfg: parameter: config.parameter.slavedb

      autoscaler: k.#Role
      autoscaler: artifact: meautoscaler
      autoscaler: cfg: parameter: config.parameter.autoscaler
    }

    connector: {
      serviceconnector: {kind: "lb"}
      //lbconnector:      {kind: "lb"}
      fullconnector:    {kind: "full"}
      workconnector:    {kind: "full"}
      dbconnector:      {kind: "full"}
    }

    link: {

      // Outside -> FrontEnd (LB connector)
			self: service: to: "serviceconnector"
      serviceconnector: to: frontend: "entrypoint"


      // Frontend, nats and worker to fullconnector
      frontend: nats: to: "fullconnector"
      fullconnector: to: nats: "nats"
      worker: nats: to: "fullconnector"

      //Worker to db
      workconnector: to: db: "db"
      worker: db: to: "workconnector"
      workconnector: to: slavedb: "slavedb"
    
   }
  }
}