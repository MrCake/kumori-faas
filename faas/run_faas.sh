#!/bin/bash

docker network create -d bridge --subnet=172.18.0.0/16 faas
./nats/run.sh 
./frontend/run.sh 
./worker/run.sh
./db/run_db.sh