package db

import k "kumori.systems/kumori/kmv"

#Manifest : k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.examples"
    name: "calccachefe"
    version: [0,0,1]
  }

  description: {

    srv: {
      duplex: db: {
        protocol: "tcp"
        port: 26257
      }
    }

    config: {
      resource: {}
      parameter: {
        config: {
          param_one : string | *"default_param_one"
          param_two : number | *"default_param_two"
        }
        calculatorEnv: string | *"default_calculatorEnv"
        restapiclientPortEnv: string | *"80"
      }
    }

    size: {
      $_memory: *"1000Mi" | uint
      $_cpu: *"500m" | uint
    }

    code: db: k.#Container & {
      name: "db"
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        tag: "mrcakedcg/db-main:0.52"
      }
      mapping: {
        filesystem: [
          {
            path: "/config/config.json"
            data: config.parameter.config
            format: "json"
          },
        ]
        env: {
          //CALCULATOR_ENV: config.parameter.calculatorEnv
          //RESTAPICLIENT_PORT_ENV: config.parameter.restapiclientPortEnv
          //SERVER_PORT_ENV: "\(srv.server.entrypoint.port)"
        }
      }
    }
  }
}
